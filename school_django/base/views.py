from django.shortcuts import render, redirect
from django.db.models import Q
from .models import Room, Topic
from .forms import RoomForm

# Create your views here.


# rooms = [
#     {'id': 1, 'name': 'Python'},
#     {'id': 2, 'name': 'Django'},
#     {'id': 3, 'name': 'Docker'},
#     {'id': 4, 'name': 'Git'},
#     {'id': 5, 'name': 'Ansible'},
# ]


def home(request):
    q = request.GET.get('q') if request.GET.get('q') != None else ''

    # this will overwrite the rooms list with the 5 entries
    rooms = Room.objects.filter(
        Q(topic__name__icontains=q) |
        Q(name__icontains=q) |
        Q(description__icontains=q)
        )

    topics = Topic.objects.all()
    room_count = rooms.count()

    context = {'rooms': rooms, 'topics': topics, 'room_count': room_count}
    return render(request, 'base/home.html', context)


# crud - read
def room(request, pk):

    # now every room will be displayed with the room name from the room template
    room = Room.objects.get(id=pk)

    # this is needed in the case for iterating over the rooms list with the 5 items
    # room = None
    # for i in rooms:
    #     if i['id'] == int(pk):
    #         room = i

    context = {'room': room}
    return render(request, 'base/room.html', context)


# crud - create
def createRoom(request):
    form = RoomForm()

    if request.method == 'POST':
        #print(request.POST)
        form = RoomForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')

    context = {'form': form}
    return render(request, 'base/room_form.html', context)


# crud - update
def updateRoom(request, pk):
    room = Room.objects.get(id=pk)
    form = RoomForm(instance=room)
    if request.method == 'POST':
        form = RoomForm(request.POST, instance=room)
        if form.is_valid():
            form.save()
            return redirect('home')

    context = {'form': form}
    return render(request, 'base/room_form.html', context)


# crud - delete
def deleteRoom(request, pk):
    room = Room.objects.get(id=pk)
    if request.method == 'POST':
        room.delete()
        return redirect('home')

    context = {'obj': room}
    return render(request, 'base/delete.html', context)
